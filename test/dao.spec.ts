import { expect } from "chai";
import { BigNumberish } from "ethers";
import { ethers } from "hardhat";
import { getProposalId } from "../helpers"

async function getTimestamp() {
  const blockNumber = await ethers.provider.getBlockNumber();
  const block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function increaseTime(seconds: number) {
  const timestamp = await getTimestamp();
  await setTimestamp(timestamp + seconds);
}

async function setTimestamp(timestamp: number) {
  await ethers.provider.send("evm_mine", [timestamp]);
}

function getTransferCallData(address: string, value: BigNumberish) {
  const abi = ["function transfer(address, uint256)"];
  const iface = new ethers.utils.Interface(abi);
  return iface.encodeFunctionData("transfer", [address, value]);
}

describe("DAO", async () => {
  let owner: any,
    account1: any,
    account2: any,
    contract: any,
    token: any,
    daoTreasury: any,
    proposalId: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("DAO");
    const Token = await ethers.getContractFactory("Token");
    [owner, account1, account2] = await ethers.getSigners();

    token = await Token.deploy();
    await token.deployed();

    contract = await Contract.deploy(owner.address, token.address, 5100, 7200);
    await contract.deployed();

    daoTreasury = await Token.deploy();
    await daoTreasury.deployed();

    await token.transfer(account1.address, ethers.utils.parseEther("10000"));
    await token.transfer(account2.address, ethers.utils.parseEther("10000"));
    await daoTreasury.transfer(
      contract.address,
      ethers.utils.parseEther("10000000")
    );
  });

  it("Quorum must be between 0 and 10000", async () => {
    const Contract = await ethers.getContractFactory("DAO");
    await expect(
      Contract.deploy(owner.address, token.address, 11000, 7200)
    ).to.be.revertedWith("Invalid quorum");
  });

  it("Deposit increases contract balance", async () => {
    await token.approve(contract.address, ethers.utils.parseEther("100"));
    await expect(() =>
      contract.deposit(ethers.utils.parseEther("100"))
    ).to.changeTokenBalance(token, contract, ethers.utils.parseEther("100"));
  });

  it("Deposit should fail if tokens are not transfered", async () => {
    await expect(
      contract.connect(account1).deposit(ethers.utils.parseEther("100"))
    ).to.be.revertedWith("ERC20: insufficient allowance");
  });

  it("Only chairman can create proposal to call contract", async () => {
    await expect(
      contract
        .connect(account1)
        .addProposal(
          getTransferCallData(account2.address, ethers.utils.parseEther("10")),
          daoTreasury.address,
          "Fund 10 token"
        )
    ).to.be.revertedWith("Only chairman adds proposal");
    await expect(
      contract.addProposal(
        getTransferCallData(account2.address, ethers.utils.parseEther("10")),
        account1.address,
        "Fund 10 token"
      )
    ).to.be.revertedWith("Recipient is not contract");
  });

  it("Adding proposal emits event", async () => {
    const callData = getTransferCallData(
      account2.address,
      ethers.utils.parseEther("10000")
    );
    proposalId = getProposalId(
      callData,
      daoTreasury.address,
      "Fund 10000 tokens"
    );

    await expect(
      contract.addProposal(callData, daoTreasury.address, "Fund 10000 tokens")
    )
      .to.emit(contract, "NewProposal")
      .withArgs(proposalId, callData, daoTreasury.address);
  });

  it("Should not vote for inactive proposal or without deposit", async () => {
    await expect(contract.vote(1, true)).to.be.revertedWith(
      "Proposal inactive"
    );
    await expect(
      contract.connect(account1).vote(proposalId, true)
    ).to.be.revertedWith("You have no deposit");
  });

  it("Should vote", async () => {
    await token
      .connect(account1)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await token
      .connect(account2)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await contract.connect(account1).deposit(ethers.utils.parseEther("100"));
    await contract.connect(account2).deposit(ethers.utils.parseEther("100"));
    await contract.connect(account1).vote(proposalId, true);
    await contract.connect(account2).vote(proposalId, false);
  });

  it("Should not vote twice", async () => {
    await contract.vote(proposalId, true);
    await expect(contract.vote(proposalId, true)).to.be.revertedWith(
      "You already voted"
    );
  });

  it("Should not finish if voting is still going", async () => {
    await expect(contract.finish(proposalId)).to.be.revertedWith(
      "Voting is still going"
    );
  });

  it("Should not withdraw if there are active proposals", async () => {
    await expect(contract.withdraw()).to.be.revertedWith(
      "You have active proposals"
    );
  });

  it("Finish should emit ProposalFinished event", async () => {
    await increaseTime(7200);
    const balanceBefore = await daoTreasury.balanceOf(account2.address);
    await expect(contract.finish(proposalId))
      .to.emit(contract, "ProposalFinished")
      .withArgs(
        proposalId,
        ethers.utils.parseEther("200"),
        ethers.utils.parseEther("100")
      );
    expect(await daoTreasury.balanceOf(account2.address)).to.eq(
      balanceBefore.add(ethers.utils.parseEther("10000"))
    );
  });

  it("Should not finish or vote on finished proposal", async () => {
    await expect(contract.vote(proposalId, true)).to.be.revertedWith(
      "Proposal inactive"
    );
    await expect(contract.finish(proposalId)).to.be.revertedWith(
      "Proposal inactive"
    );
  });

  it("Withdraw should transfer tokens", async () => {
    await expect(() => contract.withdraw()).to.changeTokenBalance(
      token,
      owner,
      ethers.utils.parseEther("100")
    );
    await contract.connect(account1).withdraw();
    await contract.connect(account2).withdraw();
  });

  it("Proposal should cancel if quorum not met", async () => {
    const callData = getTransferCallData(
      contract.address,
      ethers.utils.parseEther("10000")
    );
    await contract.addProposal(
      callData,
      daoTreasury.address,
      "Fund another 10000 tokens"
    );
    proposalId = getProposalId(
      callData,
      daoTreasury.address,
      "Fund another 10000 tokens"
    );
    await token.approve(contract.address, ethers.utils.parseEther("100"));
    await token
      .connect(account1)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await token
      .connect(account2)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await contract.deposit(ethers.utils.parseEther("100"));
    await contract.connect(account1).deposit(ethers.utils.parseEther("100"));
    await contract.connect(account2).deposit(ethers.utils.parseEther("100"));
    await contract.vote(proposalId, true);
    await contract.connect(account1).vote(proposalId, false);
    await contract.connect(account2).vote(proposalId, false);
    await increaseTime(7200);
    await expect(contract.finish(proposalId))
      .to.emit(contract, "ProposalCancelled")
      .withArgs(
        proposalId,
        ethers.utils.parseEther("100"),
        ethers.utils.parseEther("200")
      );
    await contract.withdraw();
    await contract.connect(account1).withdraw();
    await contract.connect(account2).withdraw();
  });

  it("Proposal should revert if recipient call failed", async () => {
    const Token = await ethers.getContractFactory("Token");
    const unknownToken = await Token.deploy();
    await unknownToken.deployed();
    const callData = getTransferCallData(
      contract.address,
      ethers.utils.parseEther("100")
    );
    await contract.addProposal(
      callData,
      unknownToken.address,
      "Fund 100 tokens"
    );
    proposalId = getProposalId(
      callData,
      unknownToken.address,
      "Fund 100 tokens"
    );
    await token.approve(contract.address, ethers.utils.parseEther("100"));
    await token
      .connect(account1)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await token
      .connect(account2)
      .approve(contract.address, ethers.utils.parseEther("100"));
    await contract.deposit(ethers.utils.parseEther("100"));
    await contract.connect(account1).deposit(ethers.utils.parseEther("100"));
    await contract.connect(account2).deposit(ethers.utils.parseEther("100"));
    await contract.vote(proposalId, true);
    await contract.connect(account1).vote(proposalId, true);
    await contract.connect(account2).vote(proposalId, false);
    await increaseTime(7200);
    await expect(contract.finish(proposalId)).to.be.revertedWith(
      "Function call failed"
    );
  });
});

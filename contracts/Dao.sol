// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Address.sol";

contract DAO is Ownable {
  enum ProposalState {
    New,
    Active,
    Cancelled,
    Finished
  }

  struct Proposal {
    bytes callData;
    address recipient;
    string description;
    uint256 supportVotes;
    uint256 againstVotes;
    uint256 voteStartDate;
    ProposalState state;
  }

  event NewProposal(
    uint256 indexed proposalId,
    bytes callData,
    address recipient
  );
  event ProposalCancelled(
    uint256 indexed proposalId,
    uint256 supportVotes,
    uint256 againstVotes
  );
  event ProposalFinished(
    uint256 indexed proposalId,
    uint256 supportVotes,
    uint256 againstVotes
  );

  IERC20 private _voteToken;
  address private _chairman;
  uint256 private _quorum;
  uint256 private _voteDuration;
  uint256 private _totalDeposits;
  mapping(uint256 => Proposal) private _proposals;
  mapping(address => uint256[]) private _participatingProposals;
  mapping(address => uint256) private _deposits;

  /**
    @param chairman - Chairman address
    @param voteToken - ERC20 token
    @param quorum - percentage of YES votes from the total supply of tokens
    @param voteDuration - vote duration in seconds

    Requirements:
      `quorum` must be between 0 and 10000 - where 10000 is 100.00%
   */
  constructor(
    address chairman,
    address voteToken,
    uint256 quorum,
    uint256 voteDuration
  ) {
    require(quorum > 0 && quorum <= 10000, "Invalid quorum");
    _chairman = chairman;
    _voteToken = IERC20(voteToken);
    _quorum = quorum;
    _voteDuration = voteDuration;
  }

  /**
    @param value - amount of governance tokens
   */
  function deposit(uint256 value) external {
    require(_voteToken.transferFrom(msg.sender, address(this), value), "Transfer failed");
    _deposits[msg.sender] += value;
    _totalDeposits += value;
  }

  /**
    Create new proposal
    @param callData - calldata
    @param recipient - contract address
    @param description - description of a proposal
   */
  function addProposal(
    bytes memory callData,
    address recipient,
    string memory description
  ) external {
    require(msg.sender == _chairman, "Only chairman adds proposal");
    require(Address.isContract(recipient), "Recipient is not contract");
    uint256 proposalId = uint256(
      keccak256(
        abi.encodePacked(callData, recipient, description)
      )
    );
    Proposal storage proposal = _proposals[proposalId];
    proposal.callData = callData;
    proposal.recipient = recipient;
    proposal.description = description;
    proposal.state = ProposalState.Active;
    proposal.voteStartDate = block.timestamp;
    emit NewProposal(proposalId, callData, recipient);
  }

  /**
    Vote for or against a proposal
    @param id - proposal id,
    @param support - true/false - support/oppose proposal
   */
  function vote(uint256 id, bool support) external {
    Proposal storage proposal = _proposals[id];
    require(
      proposal.voteStartDate + _voteDuration > block.timestamp &&
      proposal.state == ProposalState.Active,
      "Proposal inactive"
    );
    require(_deposits[msg.sender] > 0, "You have no deposit");
    require(!voted(msg.sender, id), "You already voted");
    if (support) {
      proposal.supportVotes += _deposits[msg.sender];
    } else {
      proposal.againstVotes += _deposits[msg.sender];
    }
    _participatingProposals[msg.sender].push(id);
  }

  function finish(uint256 id) external {
    Proposal storage proposal = _proposals[id];
    require(proposal.state == ProposalState.Active, "Proposal inactive");
    require(
      proposal.voteStartDate + _voteDuration < block.timestamp,
      "Voting is still going"
    );

    if (_totalDeposits / 10000 * _quorum >= proposal.supportVotes) {
      emit ProposalCancelled(id, proposal.supportVotes, proposal.againstVotes);
      proposal.state = ProposalState.Cancelled;
    } else {
      emit ProposalFinished(id, proposal.supportVotes, proposal.againstVotes);
      proposal.state = ProposalState.Finished;
      (bool success, ) = proposal.recipient.call(proposal.callData);
      require(success, "Function call failed");
    }
  }

  /**
    withdraw unused deposit
   */
  function withdraw() external {
    require(!hasActiveProposal(msg.sender), "You have active proposals");
    uint256 withdrawable = _deposits[msg.sender];
    _deposits[msg.sender] = 0;
    _totalDeposits -= withdrawable;
    _voteToken.transfer(msg.sender, withdrawable);
  }

  /**
    check if user has participated in a proposal
   */
  function voted(address voter, uint256 proposal) internal view returns (bool) {
    for (uint256 i = 0; i < _participatingProposals[voter].length; ++i) {
      if (_participatingProposals[voter][i] == proposal) {
        return true;
      }
    }
    return false;
  }

  /**
    check if user participates in a active proposal
   */
  function hasActiveProposal(address voter) internal view returns (bool) {
    for (uint256 i = 0; i < _participatingProposals[voter].length; ++i) {
      Proposal storage proposal = _proposals[_participatingProposals[voter][i]];
      if (proposal.state == ProposalState.Active) {
        return true;
      }
    }
    return false;
  }
}

import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  proposalId: string;
}

task("finish")
  .addParam("proposalId", "Proposal ID")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.finish(taskArgs.proposalId);
  });

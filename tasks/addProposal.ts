import { task } from "hardhat/config";
import { getContract, getProposalId } from "../helpers";

interface TaskArgs {
  calldata: string;
  recipient: string;
  description: string;
}

task("addProposal")
  .addParam("calldata", "Calldata")
  .addParam("recipient", "Contract address")
  .addParam("description", "description of a proposal")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.addProposal(
      taskArgs.calldata,
      taskArgs.recipient,
      taskArgs.description
    );
    console.log(
      "Proposal ID:",
      getProposalId(taskArgs.calldata, taskArgs.recipient, taskArgs.description)
    );
  });

import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  id: string;
  support: Boolean;
}

task("vote")
  .addParam("id", "Proposal ID")
  .addParam("support", "Support or oppose proposal")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.vote(taskArgs.id, taskArgs.support);
    console.log("Your vote counted.");
  });

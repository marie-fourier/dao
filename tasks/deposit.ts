import { task } from "hardhat/config";
import { getContract, getTokenContract } from "../helpers";

interface TaskArgs {
  value: number;
}

task("deposit")
  .addParam("value", "Amount of governance tokens")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    const governanceToken = await getTokenContract(hre);
    const value = hre.ethers.utils.parseEther(taskArgs.value.toString());
    await governanceToken.approve(contract.address, value);
    await contract.deposit(value);
    console.log("Deposited governance tokens");
  });

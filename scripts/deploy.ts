import { ethers } from "hardhat";
require("dotenv").config();

async function main() {
  const [owner] = await ethers.getSigners();
  const Contract = await ethers.getContractFactory("DAO");
  const contract = await Contract.deploy(
    owner.address,
    String(process.env.GOVERNANCE_TOKEN_ADDRESS),
    5100,
    3 * 60 * 60 * 24
  );
  await contract.deployed();

  console.log("Contract deployed to:", contract.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
